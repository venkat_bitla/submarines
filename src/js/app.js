var pubnub;
var controlRoomChannel = "control-room-channel";
var callBackChannel = "callback-channel";

const REGISTER = "REGISTER";
const NAMEINUSEE = "NAMEINUSEE";
const HIDE = "HIDE";

function registerPubNub() {
  pubnub = new PubNub({
    publishKey: "pub-3b9eaecb-fd8d-4f42-a09a-d47b6ff86e9d",
    subscribeKey: "sub-1e9d5297-fc44-11df-876f-c9d755de5a73",
  });
}

function handleControlRoomOperations() {
  pubnub.addListener({
    message: function (message) {
      var payload = message.message;
      if (payload.action === REGISTER) {
        var nameInUse = registerSubmarine(payload.submarine);
        if (nameInUse) {
          publishNameAlreadyExists(payload.submarine);
        }
      } else if (payload.action === HIDE) {
        removeSubmarineFromList(payload.submarine);
      }
    },
  });
  pubnub.subscribe({
    channels: [controlRoomChannel],
  });
}

function handleCallbackOperations() {
  pubnub.addListener({
    message: function (message) {
      var payload = message.message;
      console.log(
        "========> payload message Action " + payload.action + " " + new Date()
      );
      if (payload.action === NAMEINUSEE) {
        showNameAlreadyInUse(payload.submarine);
      } else if (payload.action === HIDE) {
        displayHideSubmarineControls(payload.submarine);
      }
    },
  });

  pubnub.subscribe({
    channels: [callBackChannel],
  });
}

function publishToRegisterSubmarine(submarine) {
  pubnub
    .publish({
      message: {
        submarine: submarine,
        action: REGISTER,
      },
      channel: controlRoomChannel,
    })
    .then((response) => {
      submarineRegisteredSuccessfully();
    })
    .catch((error) => {
      console.log("======> Error occurred while registering the submarine ");
      console.dir(error);
    });
}

function publishNameAlreadyExists(submarine) {
  pubnub
    .publish({
      message: {
        submarine: submarine,
        action: NAMEINUSEE,
      },
      channel: callBackChannel,
    })
    .then((response) => {})
    .catch((error) => {
      console.log(
        "======> Error occurred whie publishing name already exists error  "
      );
      console.dir(error);
    });
}

function publishToHideSubmarine(submarine) {
  pubnub
    .publish({
      message: {
        submarine: submarine,
        action: HIDE,
      },
      channel: controlRoomChannel,
    })
    .then((response) => {
      submarineHiddenSuccessfully();
    })
    .catch((error) => {
      console.log(
        "======> Error occurred while publishing to hide submarine from submarine page "
      );
      console.dir(error);
    });
}

// Publish request from control room to submarine to hide it
function publishRequestToHideSubmarine(submarine) {
  pubnub
    .publish({
      message: {
        submarine: submarine,
        action: HIDE,
      },
      channel: callBackChannel,
    })
    .then((response) => {
      submarineHiddenSuccessfully();
    })
    .catch((error) => {
      console.log(
        "======> Error occurred while requseting to hide the submarine from control room  "
      );
      console.dir(error);
    });
}
